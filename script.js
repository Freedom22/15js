//----------------------------------------- 1-------------------------------


document.querySelector('.btn');
addEventListener('click', function(){
    let div = document.querySelector('.text');
    div.textContent = "Операція виконана"
      
    setTimeout(function(){
      div.textContent = "Операція успішна!"
    }, 3000);
})


//--------------------------- 2 ----------------------------


const timeElement = document.getElementById('time');
let time = 10;

const timeInterval = setInterval(function(){
    if (time > 1) {
        timeElement.textContent = time;
        time--;
    }else {
        clearInterval(timeInterval);
        timeElement.textContent = 'Відлік завершено'
    }
}, 1000);